package com.RFT.steven;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Character extends Rectangle{
	public Color col = Color.red;
	public Character(int sx, int sy, int cSize){
		super.setSize(cSize, cSize);
		super.setLocation(sx, sy);
		
	}
	public void chkBounds(){
		//method for bounds checking for the character on the window.
		if(this.getX()<0){
			this.x = 0;
		}
		if(this.getX()+36>Main.screenSize){
			this.x = Main.screenSize-36;
		}
		if(this.getY()<0){
			this.y = 0;
		}
		if(this.getY()+56>Main.screenSize){
			this.y = Main.screenSize-56;
		}
		
	}
	public Color getCol() {
		return col;
	}
	public void setCol(Color col) {
		this.col = col;
	}
}
