package com.RFT.steven;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;

public class RFTjp extends JPanel{
	public static int delay = 50;
	public void paint(Graphics g){
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Main.man.getCol());
		g2.fill(Main.man);
		
	}
	public RFTjp(){
		setBackground(Color.DARK_GRAY);
	    setDoubleBuffered(true);
	    new Thread(new Runnable(){
			@Override
			public void run() {
				long bt, td, sl;
				bt = System.currentTimeMillis();
				while(true){
					td = System.currentTimeMillis()-bt;
					sl = delay - td;
					Main.man.chkBounds();
					repaint();
					if(sl <0){
						sl = 2;
						try{
							Thread.sleep(sl);
						}catch(InterruptedException e){
							System.out.println(e.toString());
						}
					}
				}
			}
	    }).start();
	}
}

