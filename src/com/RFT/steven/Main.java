package com.RFT.steven;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Main extends JFrame {
	private static final long serialVersionUID = 1L;
	public static RFTjp contP;
	public static Main frame;
	public static int manSize = 30;
	public static Character man = new Character(0,0,manSize);
	public static int screenSize = 780;
	public static int tS = 30;
	public static int speed = 30;
	
 public static void main(String[] args){
		new Main();
		
	}
	public Main(){
		//setting up the window and drawing surface.
		this.setResizable(false);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("The Rotting Flowered Terrace");
		this.setBounds(680, 230, screenSize,screenSize);
		contP = new RFTjp();
		contP.setBounds(680, 230, screenSize, screenSize);
		this.add(contP);
		this.setContentPane(contP);
		this.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent arg0) {
				switch(arg0.getKeyCode()){
				case KeyEvent.VK_A:
					man.setLocation((int)man.getX()-speed, (int)man.getY());
					break;
				case KeyEvent.VK_D:
					man.setLocation((int)man.getX()+speed, (int)man.getY());
					break;
				case KeyEvent.VK_W:
					man.setLocation((int)man.getX(), (int)man.getY()-speed);
					break;
				case KeyEvent.VK_S:
					man.setLocation((int)man.getX(), (int)man.getY()+speed);
					break;
				}
			}
			@Override
			public void keyReleased(KeyEvent arg0) {
			
			}
			@Override
			public void keyTyped(KeyEvent arg0) {
				
			}
		});
		new Thread(new Runnable(){
			//debugging stuff.
			@Override
			public void run() {
				while(true){
					System.out.println(man.getX()+" "+man.getY());
				}
			}
		}).start();
		
	}
}
